// variables

//global scope

//block scope and redeclare

const variable1 = "kalidas"; //storage

{
  const variable1 = "amal";
  console.log(variable1);
}

{
  const variable1 = "kalidas rajeev";
  console.log(variable1);
}

console.log(variable1);
