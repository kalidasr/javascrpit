const array = [1, 2, 3, 4, 5]; //collection of similar items

const obj = {
  name: "kalidas",
  lastname: "rajeev",
  age: 25,
  skills: ["javascript", "react"],
};

console.log(array.findIndex((item) => item === 23));

// array.map((item) => {
//   if (item < 4) console.log(item);
// });

// console.log(array.map((item) => item * item));

// even
// console.log(array.filter((item) => item % 2 === 0).map((item) => item * 2));

// console.log(Object.keys(obj));
// console.log(Object.values(obj));

// const array_obj = [
//   { name: "kalidas", designation: "react developer" },
//   { name: "amal", designation: "ui developer" },
// ];

// const new_array = array_obj.filter((item) => {
//   return item.name.charAt(0) === "a" || item.designation === "react developer";
// });

// console.log(new_array);

const nameValue = "kalidas";

console.log(nameValue.split(""));
console.log(nameValue.includes("kal"));

const array_new = {
  id: "0001",
  type: "donut",
  name: "Cake",
  ppu: 0.55,
  batters: {
    batter: [
      { id: "1001", type: "Regular" },
      { id: "1002", type: "Chocolate" },
      { id: "1003", type: "Blueberry" },
      { id: "1004", type: "Devil's Food" },
    ],
  },
  topping: [
    { id: "5001", type: "None" },
    { id: "5002", type: "Glazed" },
    { id: "5005", type: "Sugar" },
    { id: "5007", type: "Powdered Sugar" },
    { id: "5006", type: "Chocolate with Sprinkles" },
    { id: "5003", type: "Chocolate" },
    { id: "5004", type: "Maple" },
  ],
};
